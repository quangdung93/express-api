const supertest = require('supertest');
const chai = require('chai');
const app = require('../index'); // Import ứng dụng Express

const { expect } = chai;
const request = supertest(app);

describe('API Tests', () => {
    it('should return a welcome message', async () => {
        const response = await request.get('/api');
        expect(response.status).to.equal(200);
        expect(response.text).to.equal('Welcome to the API');
    });

    it('should login success', async () => {
        const response = await request.post('/api/login').send({
            username: 'dungvq11',
            password: '012087',
        });

        expect(response.status).to.equal(200);
        expect(response.body.error).to.equal(0);
        expect(response.body.data).to.have.property('token');
        expect(response.body.data.token).to.exist;
        
        expect(response.body.data).to.have.property('user');
        expect(response.body.data.user).to.exist;
    });

    it('should create a new user', async () => {
        const response = await request.post('/api/users/create').send({
            username: 'dungvq12',
            email: 'dungvq12@example.com',
            password: '012087',
        });

        console.log(response.body);

        expect(response.status).to.equal(200);
        expect(response.body.error).to.equal(0);
        
        expect(response.body.data).to.have.property('user');
        expect(response.body.data.user).to.exist;
    });
});
