const winston = require('winston');
require('winston-daily-rotate-file');
const path = require('path');

module.exports = winston.createLogger({
    format: winston.format.combine(
        winston.format.splat(),
        // Định dạng time cho log
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        // thêm màu sắc
        // winston.format.colorize(),
        // thiết lập định dạng của log
        winston.format.printf(
            log => {
                // nếu log là error hiển thị stack trace còn không hiển thị message của log 
                // if(log.stack) return `[${log.timestamp}] [${log.level}] ${log.stack}`;
                return  `[${log.timestamp}] [${log.level}] ${log.message} - ${JSON.stringify(log.context)}`;
            },
        ),
    ),
    transports: [
        // hiển thị log thông qua console
        new winston.transports.Console(),

        //Log mỗi ngày
        new winston.transports.DailyRotateFile ({
            level: 'info',
            filename: path.join(__dirname, 'logs', `%DATE%.log`),
            datePattern: 'YYYY-MM-DD',
            handleExceptions: false,
            colorize: true,
            json: true,
            prepend: true,
            zippedArchive: true,
            auditFile: path.join(__dirname, 'logs/audit/audit.json'),
            maxSize: '10m',
            maxFiles: '10d'        
        }),

        new winston.transports.DailyRotateFile ({
            level: 'error',
            filename: path.join(__dirname, 'logs', `%DATE%.log`),
            datePattern: 'YYYY-MM-DD',
            handleExceptions: false,
            colorize: true,
            json: true,
            prepend: true,
            zippedArchive: true,
            auditFile: path.join(__dirname, 'logs/audit/audit.json'),
            maxSize: '10m',
            maxFiles: '10d'        
        })
    ],
})

