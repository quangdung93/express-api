const { Sequelize } = require('sequelize');

const apiError = require('../utils/apiError');
const { storeLog } = require("../utils/Helpers");

const errorHandler = (err, req, res, next) => {
        // Xử lý logic các loại lỗi
        if (err instanceof apiError) {

            let response = {
                error: 1,
                error_code: err?.errorCode,
                message: err?.message,
                ex_message: err?.exMessage,
                data: err?.data
            }

            //Lưu log
            storeLog(req, response);

            res.status(err.statusCode).json(response);
        }
        else if (err instanceof Sequelize.ValidationError) {
            // Lấy danh sách các lỗi con
            const errors = err.errors;
            const errorMessages = errors.map((err) => err.message);
            
            //Lưu log
            storeLog(req, errorMessages.join('|'));

            res.status(400).json({
                error: 1,
                message: errorMessages.join('|')
            });
        } else {
            //Lưu log
            storeLog(req, err?.message);

            // Xử lý các lỗi khác mặc định
            res.status(500).json({
                error: 1,
                message:  err.message || 'Internal Server Error'
            });
        }
};

module.exports = errorHandler;
