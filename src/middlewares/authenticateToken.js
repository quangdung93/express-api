const jwt = require('jsonwebtoken');
const responseFormat = require('../utils/responseFormat');

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];

    if (!token) {
        return res.status(200).json(responseFormat.error('Missing access_token', 401));
    }

    jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
        if (err) {
            return res.status(200).json(responseFormat.error('Invalid access_token', 403));
        }

        req.user = decoded; // Lưu thông tin người dùng từ decoded vào request
        next();
    });
};

module.exports = authenticateToken;