const logging = require('../../logging');
const crypto = require('crypto');
const moment = require('moment');

exports.tryCatch = (controller) => async (req, res, next) => {
    try {
        await controller(req, res);
    } catch (error) {
        return next(error);
    }
};

exports.storeLog = (req, res) => {
    try {
        logging.error(`${req.method} ${req.url}`, {context: {request: req.body, response: res}});
    } catch (error) {}
};

exports.loggingApi = (req, res) => {
    try {
        logging.info(`${req.method} ${req.url}`, {context: {request: req.body, response: res}});
    } catch (error) {}
};

exports.generateRandomNumber = () => {
    const min = 100000; // Min
    const max = 999999; // Max
    return Math.floor(Math.random() * (max - min + 1) + min);
};

exports.updateOrCreate  = async (model, where, newItem) => {
    // First try to find the record
    const foundItem = await model.findOne({where});
    if (!foundItem) {
        // Item not found, create a new one
        const item = await model.create({...newItem, ...where})
        return  {item, created: true};
    }
    // Found an item, update it
    const item = await model.update(newItem, {where});
    return {item, created: false};
}

exports.getNextTime = (numberNext = 1, format = 'YYYY-MM-DD HH:mm:ss', type = 'minutes') => {
    const currentTime = moment();
    const otpExpired = currentTime.add(numberNext, type);
    const otpExpiredFormat = otpExpired.format(format);

    return otpExpiredFormat;
};


