class apiError extends Error {
    constructor(errorCode, message, exMessage = '', data = null, statusCode = 200) {
        super(message);
        this.exMessage = exMessage;
        this.errorCode = errorCode;
        this.data = data;
        this.statusCode = statusCode;
    }
}
module.exports = apiError;

