const responseFormat = {
    /**
     * Trả về một Response thành công với dữ liệu được định dạng theo JSON
     * @param {Object} data
     * @param {Number} statusCode
     * @returns {Object}
     */
    success(data, message = '') {
        return {
            error: 0,
            message: message || 'Xử lý thành công!',
            data
        };
    },

    /**
     * Trả về một Response lỗi với thông báo lỗi
     * @param {String} message
     * @param {Number} statusCode
     * @returns {Object}
     */
    error(message = '', errorCode = -1, exMessage = '', dataError = null) {
        return {
            error: 1,
            error_code: errorCode,
            message: message || 'Xử lý thất bại!',
            ex_message: exMessage,
            data: dataError
        };
    }
};

module.exports = responseFormat;
