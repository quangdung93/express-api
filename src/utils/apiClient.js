const axios = require('axios');
const { storeLog } = require("./Helpers");

const apiClient = axios.create({
    baseURL: process.env.API_DOMAIN,
    timeout: 20000,
    headers: {
        'Content-Type': 'application/json',
        // Các tiêu đề khác (nếu cần)
    },
});

apiClient.interceptors.request.use((config) => {
    // Thêm thông tin định danh, tiêu đề chung, xử lý request trước khi gọi API

    return config;
}, (error) => {
    return Promise.reject(error);
});

apiClient.interceptors.response.use((response) => {
    // Xử lý response trước khi trả về cho người dùng
    
    return response;
}, (error) => {
    // Xử lý lỗi response và log lỗi (tùy chọn)
    return Promise.reject(error);
});

module.exports = apiClient;
