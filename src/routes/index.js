const express = require('express');
const router = express.Router();
require('express-group-routes');
const { tryCatch } = require("../utils/Helpers");
const { validateCreateCustomer } = require('../requests/customer/createCustomer.request');
const { validateLogin } = require('../requests/authen/login.request');

const customerController = require('../controllers/customer.controller');
const bannerController = require('../controllers/banner.controller');
const storeController = require('../controllers/store.controller');
const transactionController = require('../controllers/transaction.controller');
const voucherController = require('../controllers/voucher.controller');
const authenController = require('../controllers/authen.controller');
const orderController = require('../controllers/order.controller');
const pointController = require('../controllers/point.controller');

const authenticateToken = require('../middlewares/authenticateToken');

// Định nghĩa các route
router.get('/', (req, res) => {
    res.send('Welcome to Fpoint API...');
});

//Login
router.post("/generate-otp", tryCatch(authenController.generateOtpAction));
router.post("/verify-otp", tryCatch(authenController.verifyOtpAction));
router.post("/login", validateLogin, tryCatch(authenController.login));
router.post("/refresh-token", tryCatch(authenController.refreshTokenAction));

//Register
router.group('/register', async function (route) {
    route.post("/", validateCreateCustomer, tryCatch(authenController.register));
    route.post("/generate-otp", tryCatch(authenController.generateOtpRegisterAction));
    route.post("/verify-otp", tryCatch(authenController.verifyOtpRegisterAction));
});


router.group('/banners', async function (route) {
    route.get("/", bannerController.getAllAction);
});

router.group('/stores', async function (route) {
    route.get("/", storeController.getAllAction);
});

//Check JWT middleware
router.use(authenticateToken);

router.group('/customers', async function (route) {
    route.get("/", customerController.getAllAction);
    route.post("/create", validateCreateCustomer, customerController.createAction);
    route.post("/update/:id", customerController.updateAction);
    route.post("/delete/:id", customerController.deleteAction);
});

router.group('/transactions', async function (route) {
    route.get("/", transactionController.getAllAction);
});


router.group('/vouchers', async function (route) {
    route.get("/", voucherController.getAllAction);
});



router.group('/orders', async function (route) {
    route.post("/", orderController.getCustomerOrderAction);
    route.post("/suborder", orderController.getSubOrderAction);
});

router.group('/points', async function (route) {
    route.post("/info", pointController.getPointAction);
    route.post("/accumulate", pointController.accumulatePointAction);
});

// Xuất router để sử dụng trong file khác
module.exports = router;
