const bcrypt = require('bcrypt');
const Customer = require('../models/customer.model');
const apiError = require('../utils/apiError');
const usersApi = require('../api/user.api');

class CustomerService {
    async getAll() {
        return Customer.findAll({
            attributes: ['id', 'name', 'username', 'email'],
            where: {status: 1}
        });
    }

    async getById(id) {
        // Xử lý logic để lấy thông tin người dùng từ cơ sở dữ liệu
        return Customer.findByPk(id);
    }

    async create(customerData) {
        let checkUsername = await Customer.findOne({ where: { username: customerData.username } });
        if(checkUsername){
            throw new apiError(400, 'Tên đăng nhập đã được đăng ký!');
        }

        let checkPhone = await Customer.findOne({ where: { phone: customerData.phone } });
        if(checkPhone){
            throw new apiError(400, 'Số điện thoại đã được đăng ký!');
        }

        let checkEmail = await Customer.findOne({ where: { email: customerData.email } });
        if(checkEmail){
            throw new apiError(400, 'Email đã được đăng ký!');
        }
        
        // Hash password
        let passwordHash = await bcrypt.hash('fpoint@pwd', 10);

        let dataCreate = {
            name: customerData.name,
            username: customerData.username,
            phone: customerData.phone,
            email: customerData.email,
            password: passwordHash,
        };

        const customer = new Customer(dataCreate);
        return customer.save();
    }

    async update(id, customerData) {
        // Xử lý logic để cập nhật thông tin người dùng trong cơ sở dữ liệu
        let dataUpdate = {
            username: customerData.username,
            email: customerData.email
        }

        return Customer.update(dataUpdate, {
            where: {id}
        });
    }

    async delete(id) {
        // Xử lý logic để xóa người dùng khỏi cơ sở dữ liệu
        return Customer.destroy({ where: { id } });
    }

    async getUserData() {
        return await usersApi.fetchUserData();
    }
}

module.exports = new CustomerService();
