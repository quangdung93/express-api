const apiError = require('../utils/apiError');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const firebase = require('firebase-admin');
const logging = require('../../logging');

const Point = require('../models/point.model');
const Customer = require('../models/customer.model');
const SubOrder = require('../models/suborder.model');
const SubOrderItem = require('../models/suborderItem.model');
const orderApi = require('../api/order.api');
const serviceAccountFCM = require('../../serviceAccountKey.json');

class PointService {

    //Xác thực điểm
    async verifyPoint(params) {
        const {suborder_id, telephone, hashcode, accumulate_source} = params;

        //Kiểm tra nguồn tích điểm
        // if(accumulate_source !== 'ecommerce'){
        //     throw new apiError(400, 'Nguồn tích điểm không hợp lệ');
        // }

        //Check xem suborder_id đã được tích điểm chưa
        let checkAccumPoint = await Point.findOne({ 
            where: { obj_id: suborder_id, status: 1 }
        });

        if(checkAccumPoint){
            throw new apiError(400, 'Đơn hàng này đã được tích điểm');
        }

        //Lấy thông tin đơn hàng từ Fahasa
        let subOrder = await orderApi.getSubOrderApi(suborder_id);
        
        if(!subOrder.success){
            throw new apiError(400, subOrder.errorMsg);
        }

        //Nếu thông tin đơn hàng không có customer_id thì báo lỗi
        if(!subOrder.customer_id){
            throw new apiError(400, 'Không tìm thấy customer_id');
        }

        //Thông tin tạo chuỗi hash
        const hashInput = {
            order_id: subOrder.order_id,
            suborder_id: subOrder.suborder_id,
            customer_id: subOrder.customer_id,
            total: subOrder.total,
        };

        //Tạo chuỗi hash
        const md5Hash = crypto.createHash('md5').update(JSON.stringify(hashInput), 'utf-8').digest('hex');
        logging.info(`Verify Hash`, {context: {request: params, md5: md5Hash}});

        //Vefiry hashcode (nếu chuỗi hashcode được gửi từ input không khớp với chuỗi được hash thì báo lỗi)
        if(hashcode != md5Hash){
            throw new apiError(400, 'Chuỗi hash không hợp lệ!');
        }

        let subOrderFpoint = await SubOrder.findOne({ 
            where: { suborder_id }
        });

        if(!subOrderFpoint){
            //Lưu đơn hàng
            const subOrderInfo = new SubOrder({
                order_id: subOrder.order_id,
                suborder_id: subOrder.suborder_id,
                customer_id: subOrder.customer_id,
                total: subOrder.total,
                order_date: subOrder.created_at,
                order_status: subOrder.status,
            }).save();

            //Lưu chi tiết đơn hàng
            if(subOrder.items){
                let newItems = [];
                for(let item of subOrder.items){
                    newItems.push({
                        order_id: subOrder.order_id,
                        suborder_id: subOrder.suborder_id,
                        product_id: item.product_id,
                        sku: item.sku,
                        name: item.name,
                        qty: item.qty,
                        price: item.price,
                    });
                }

                if(newItems.length){
                    SubOrderItem.bulkCreate(newItems);
                }
            }
        }

        //Lấy thông tin khách hàng từ Fahasa
        let ecommerceCustomer = await orderApi.getCustomerOrderApi(telephone);

        //Lấy KH tại fpoint
        let customerFpoint = await Customer.findOne({ 
            where: { phone: telephone }
        });

        //Tính level KH
        let level = await this.calculateLevel(subOrder);

        //Nếu KH chưa tồn tại thì tạo mới
        let customerInfo = {};
        if(!customerFpoint){
            // Hash password
            let passwordHash = await bcrypt.hash(telephone, 10);

            customerInfo = new Customer({
                ecom_customer_id: ecommerceCustomer?.customer_id,
                email: ecommerceCustomer?.email,
                level: level,
                phone: telephone,
                first_name: ecommerceCustomer?.first_name,
                name: ecommerceCustomer?.last_name,
                username: telephone,
                password: passwordHash
            }).save();

            customerFpoint = customerInfo;
        }
        else{
            customerFpoint.level = level;
            customerFpoint.ecom_customer_id = ecommerceCustomer?.customer_id;
            customerInfo = await customerFpoint.save();
        }
        //

        //Tiến hành tích điểm
        return await this.accumulatePoint(subOrder, customerInfo, level, accumulate_source);
    }

    //Tích điểm
    async accumulatePoint(subOrder, customerInfo, level, accumulate_source){
        // Config phần trăm cộng điểm theo hạng
        const customerLevel = {
            member: 0,
            bronze: 0.5,
            silver: 1,
            gold: 2,
        };

        //Tính số phần trăm theo level
        let percent = customerLevel[level];

        //Tính số điểm được cộng
        const accumulatePoint = Math.round(subOrder.total * percent/100);

        // Lưu điểm cộng
        let prevPoint = await Point.findOne({ 
            where: { customer_id: subOrder.customer_id, status: 1 }, 
            order: [['id', 'DESC']]
        });

        const currentDate = new Date();
        const day = currentDate.getDate();
        const month = currentDate.getMonth() + 1;
        const year = currentDate.getFullYear();

        let dataCreate = {
            customer_id: subOrder.customer_id,
            action: 'plus',
            obj_id: subOrder.suborder_id,
            source: accumulate_source,
            description: `Tích điểm đơn hàng ${subOrder.suborder_id}`,
            point_date: day,
            point_month: month,
            point_year: year,
        };

        if(prevPoint){
            dataCreate.point_before = prevPoint.point_available;
            dataCreate.point = accumulatePoint;
            dataCreate.point_available = prevPoint.point_available + accumulatePoint;
        }
        else{
            dataCreate.point_before = 0;
            dataCreate.point = accumulatePoint;
            dataCreate.point_available = accumulatePoint;
        }

        const point = new Point(dataCreate).save();

        //Send notification
        if(customerInfo && customerInfo.device_token){
            const deviceToken = customerInfo.device_token;
            const notificationTitle = 'Thông báo tích điểm';
            const notificationBody = `Chúc mừng bạn vừa được cộng ${accumulatePoint} điểm cho giao dịch ${subOrder.suborder_id}`;
            this.sendNotification(deviceToken, notificationTitle, notificationBody);   
        }

        return point;
    }

    async sendNotification(deviceToken, title, body){
        // Khởi tạo Firebase Admin SDK với config
        firebase.initializeApp({
            credential: firebase.credential.cert(serviceAccountFCM),
        });

        const message = {
            token: deviceToken,
            notification: {
                title: title,
                body: body,
            },
        };
    
        firebase.messaging().send(message)
            .then((response) => {
                console.log('Notification sent successfully:', response);
            })
            .catch((error) => {
                console.error('Error sending notification:', error);
            });
    }

    async getPoint(customerId){
        let point = await Point.findOne({ where: { customer_id: customerId, status: 1 }, order: [['id', 'DESC']]});

        if(!point){
            throw new apiError(200, 'Người dùng này chưa có thông tin tích điểm!');
        }

        return {point: point.point_available};
    }

    async calculateLevel(subOrder){
        let allOrderByCustomer = await SubOrder.findAll({ 
            where: { customer_id: subOrder.customer_id }
        });

        let totalBuy = 0;

        for(let order of allOrderByCustomer){
            totalBuy += order.total;
        }

        let level = 'member';
        if(0 < totalBuy && totalBuy <= 400000){
            level = 'bronze';
        }
        else if(400000 < totalBuy && totalBuy <= 2000000){
            level = 'silver';
        }
        else{
            level = 'gold';
        }

        return level;
    }
}

module.exports = new PointService();
