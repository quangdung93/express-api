const bcrypt = require('bcrypt');
const Customer = require('../models/customer.model');
const Point = require('../models/point.model');
const OtpLog = require('../models/otpLog.model');
const apiError = require('../utils/apiError');
const moment = require('moment');
const jwt = require('jsonwebtoken');
const { generateRandomNumber } = require("../utils/Helpers");
const smsApi = require('../api/sms.api');
const { Sequelize } = require('sequelize');
const { updateOrCreate, getNextTime } = require("../utils/Helpers");

class CustomerService {
    async authenticateUser(username, password, device_token) {
        const customer = await Customer.findOne({ where: { username } });

        let fpoint;
        if(customer.ecom_customer_id){
            fpoint = await Point.findOne({where: {customer_id: customer.ecom_customer_id, status: 1}, order: [['id', 'DESC']]});
        }

        if(!customer){
            throw new apiError(400, 'Tên đăng nhập hoặc mật khẩu không đúng!');
        }

        //Cập nhật device_token
        if(device_token){
            customer.device_token = device_token;
            customer.save();
        }

        let result = await bcrypt.compare(password, customer.password);

        if(result){
            return {
                id: customer.id,
                name: customer.name,
                username: customer.username,
                code: customer.code,
                phone: customer.phone,
                device_token: customer.device_token,
                fpoint: fpoint ? fpoint.point_available : 0,
                ecom_customer_id: customer.ecom_customer_id,
                status: customer.status,
                created_at: customer.created_at
            }
        }
        
        throw new apiError(400, 'Tên đăng nhập hoặc mật khẩu không đúng!');
    }

    async generateToken(customer){
        // Tạo payload cho JWT
        const payload = {
            user_id: customer.id,
            username: customer.username
        };

        // Tạo JWT
        const token = jwt.sign(payload, process.env.SECRET_KEY, {expiresIn: '24h'});
        const refreshToken = jwt.sign(payload, process.env.SECRET_KEY, {expiresIn: '7d'});

        let update = await Customer.update({access_token: token, refresh_token: refreshToken}, {
            where: {id: customer.id}
        });

        if(!update){
            throw new apiError(400, 'Lưu token thất bại!');
        }

        return {user: customer, access_token: token, refresh_token: refreshToken}
    }

    async refreshToken(refreshToken){
        if(!refreshToken){
            throw new apiError(401, 'Missing Refresh Token');
        }
    
        let dataToken = jwt.verify(refreshToken, process.env.SECRET_KEY, async (err, decoded) => {
            if (err) {
                throw new apiError(401, 'Invalid Refresh Token');
            }
    
            const customer = await Customer.findOne({ where: { id: decoded.user_id, refresh_token: refreshToken }});

            if(!customer){
                throw new apiError(401, 'Invalid Refresh Token');
            }

            if(customer.ecom_customer_id){
                const fpoint = await Point.findOne({where: {customer_id: customer.ecom_customer_id, status: 1}, order: [['id', 'DESC']]});
                customer.fpoint = fpoint ? fpoint.point_available : 0;
            }
    
            let dataToken = await this.generateToken(customer);
    
            return dataToken;
        });

        return dataToken;
    }

    async generateOtp(phone) {

        //Kiểm tra sdt đã đăng ký chưa?
        const customer = await Customer.findOne({ where: { phone } });
        if(!customer){
            throw new apiError(403, 'Số điện thoại chưa được đăng ký!');
        }

        //Generate OTP
        const OTP = generateRandomNumber();

        //Send SMS
        const contentSms = `FAHASA.COM gui ban ma OTP xac nhan tai khoan: ${OTP}`;

        if(process.env.NODE_ENV !== 'development'){
            let sendSms = await smsApi.sendSms({phone, content: contentSms});

            if(!sendSms){
                return false;
            }
        }

        // Thêm 5 phút vào thời gian hiện tại
        const otpExpired = getNextTime(5);

        let dataUpdate = {
            otp: OTP,
            otp_expired: otpExpired
        };

        Customer.update(dataUpdate, {
            where: {phone}
        });

        await updateOrCreate(OtpLog, {phone}, {...dataUpdate, content: contentSms});

        return true;
    }

    async verifyOtp(phone, otp) {
        const currentTime = moment().format('YYYY-MM-DD HH:mm:ss');

        //Kiểm tra sdt đã đăng ký chưa?
        const customer = await Customer.findOne({
            where: {phone, otp} 
        });

        if(!customer){
            throw new apiError(101, 'Mã OTP không đúng!');
        }

        let fpoint;
        if(customer.ecom_customer_id){
            fpoint = await Point.findOne({where: {customer_id: customer.ecom_customer_id, status: 1}, order: [['id', 'DESC']]});
        }

        if(customer.otp_expired < currentTime){
            throw new apiError(102, 'Mã OTP đã hết hạn!');
        }

        return {
            id: customer.id,
            name: customer.name,
            username: customer.username,
            code: customer.code,
            phone: customer.phone,
            fpoint: fpoint ? fpoint.point_available : 0,
            ecom_customer_id: customer.ecom_customer_id,
            status: customer.status,
            created_at: customer.created_at
        }
    }

    async generateOtpRegister(phone) {
        //Generate OTP
        const OTP = generateRandomNumber();

        //Send SMS
        const contentSms = `FAHASA.COM gui ban ma OTP dang ky tai khoan: ${OTP}`;
        let sendSms = await smsApi.sendSms({phone, content: contentSms});

        if(!sendSms){
            return false;
        }

        let dataUpdate = {
            otp: OTP,
            otp_expired: getNextTime(5) //Lấy 5phút kế tiếp
        };

        await updateOrCreate(OtpLog, {phone}, {...dataUpdate, content: contentSms});

        return true;
    }

    async verifyOtpRegister(phone, otp) {
        const currentTime = moment().format('YYYY-MM-DD HH:mm:ss');

        //Kiểm tra sdt đã đăng ký chưa?
        const otpLog = await OtpLog.findOne({
            where: {phone, otp} 
        });

        if(!otpLog){
            throw new apiError(101, 'Mã OTP không đúng!');
        }

        if(otpLog.otp_expired < currentTime){
            throw new apiError(102, 'Mã OTP đã hết hạn!');
        }

        return true;
    }
}

module.exports = new CustomerService();
