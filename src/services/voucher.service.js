const Voucher = require('../models/voucher.model');

class VoucherService {

    async getAll() {
        return Voucher.findAll({
            attributes: ['id', 'code', 'name', 'image', 'scope', 'using_date', 'expired_date'],
            where: {status: 1}
        });
    }
}

module.exports = new VoucherService();
