const bcrypt = require('bcrypt');
const apiError = require('../utils/apiError');
const { generateRandomNumber } = require("../utils/Helpers");
const orderApi = require('../api/order.api');
const crypto = require('crypto');

class OrderService {

    async getCustomerOrder(params) {
        let response = await orderApi.getCustomerOrderApi(params);

        if(!response.success){
            throw new apiError(400, response.errorMsg);
        }
        
        return response;
    }

    async getSubOrder(suborder_id) {
        let response = await orderApi.getSubOrderApi(suborder_id);
        
        // if(!response.success){
        //     throw new apiError(400, response.errorMsg);
        // }
        
        return response;
    }
}

module.exports = new OrderService();
