const Store = require('../models/store.model');

class StoreService {

    //Có Phân trang
    // async getAll(params) {
    //     let { page } = params;
    //     page = page || 1;
    //     const pageSize = 10;
    //     const offset = (page - 1) * pageSize;

    //     const {count, rows} = await Store.findAndCountAll({
    //         limit: pageSize,
    //         offset: offset,
    //         attributes: ['id', 'code', 'name', 'address', 'open_time', 'image', 'latitude', 'longitude'],
    //         where: {status: 1},
    //         order: [['id', 'ASC']]
    //     });

    //     const totalPages = Math.ceil(count / pageSize);

    //     return {
    //         paging: {count, page, totalPages},
    //         rows
    //     };
    // }

    async getAll(params) {
        let { page } = params;

        return await Store.findAll({
            attributes: ['id', 'code', 'name', 'address', 'open_time', 'image', 'latitude', 'longitude'],
            where: {status: 1},
            order: [['id', 'ASC']]
        });
    }
}

module.exports = new StoreService();
