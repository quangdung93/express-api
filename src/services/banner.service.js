const bcrypt = require('bcrypt');
const Banner = require('../models/banner.model');

class BannerService {

    async getAll() {
        return Banner.findAll({
            attributes: ['id', 'title', 'content', 'url', 'image'],
            where: {status: 1}
        });
    }

    async getById(id) {
        // Xử lý logic để lấy thông tin người dùng từ cơ sở dữ liệu
        return Banner.findByPk(id);
    }

    async create(BannerData) {
        // Xử lý logic để tạo mới người dùng trong cơ sở dữ liệu
        let passwordHash = await bcrypt.hash(BannerData.password, 10);
        BannerData.password = passwordHash;
        const Banner = new Banner(BannerData);
        return Banner.save();
    }

    async update(id, BannerData) {
        // Xử lý logic để cập nhật thông tin người dùng trong cơ sở dữ liệu
        let dataUpdate = {
            username: BannerData.username,
            email: BannerData.email
        }

        return Banner.update(dataUpdate, {
            where: {id}
        });
    }

    async delete(id) {
        // Xử lý logic để xóa người dùng khỏi cơ sở dữ liệu
        return Banner.destroy({ where: { id } });
    }
}

module.exports = new BannerService();
