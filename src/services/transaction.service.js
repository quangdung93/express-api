const Transaction = require('../models/transaction.model');

class TransactionService {

    async getAll() {
        return Transaction.findAll({
            attributes: ['id', 'title', 'amount', 'type', 'date', 'address'],
            where: {status: 1}
        });
    }
}

module.exports = new TransactionService();
