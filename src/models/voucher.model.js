const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Voucher = sequelize.define('vouchers', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    code: {type: DataTypes.STRING},
    name: {type: DataTypes.STRING},
    image: {type: DataTypes.STRING},
    scope: {type: DataTypes.ENUM('all','ecommerce', 'store')},
    using_date: {type: DataTypes.DATE},
    expired_date: {type: DataTypes.DATE},
    status: {type: DataTypes.STRING, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Voucher;