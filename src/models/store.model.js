const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Store = sequelize.define('stores', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING},
    address: {type: DataTypes.STRING},
    image: {type: DataTypes.STRING},
    open_time: {type: DataTypes.DATE},
    latitude: {type: DataTypes.STRING},
    longitude: {type: DataTypes.STRING},
    status: {type: DataTypes.STRING, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Store;