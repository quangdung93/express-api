const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Customer = sequelize.define('customers', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    first_name: {type: DataTypes.STRING},
    name: {type: DataTypes.STRING},
    ecom_customer_id: {type: DataTypes.STRING},
    level: {type: DataTypes.STRING, defaultValue: 'member'},
    username: {type: DataTypes.STRING, unique: true},
    password: {type: DataTypes.STRING},
    email: {type: DataTypes.STRING, unique: true},
    code: {type: DataTypes.STRING},
    phone: {type: DataTypes.STRING},
    device_token: {type: DataTypes.STRING},
    otp: {type: DataTypes.STRING},
    otp_expired: {type: DataTypes.DATE},
    fpoint: {type: DataTypes.INTEGER},
    source: {type: DataTypes.ENUM('fpoint','ecommerce','pos','other')},
    access_token: {type: DataTypes.STRING},
    refresh_token: {type: DataTypes.STRING},
    status: {type: DataTypes.INTEGER, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Customer;