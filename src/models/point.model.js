const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Point = sequelize.define('points', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    customer_id: {type: DataTypes.STRING},
    action: {type: DataTypes.ENUM('plus','minus')},
    obj_id: {type: DataTypes.INTEGER},
    source: {type: DataTypes.ENUM('ecommerce','pos')},
    description: {type: DataTypes.TEXT},
    point_before: {type: DataTypes.INTEGER, defaultValue: 0},
    point: {type: DataTypes.INTEGER, defaultValue: 0},
    point_available: {type: DataTypes.INTEGER, defaultValue: 0},
    point_date: {type: DataTypes.INTEGER},
    point_month: {type: DataTypes.INTEGER},
    point_year: {type: DataTypes.INTEGER},
    status: {type: DataTypes.INTEGER, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Point;