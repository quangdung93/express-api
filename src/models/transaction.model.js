const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Transaction = sequelize.define('transactions', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    title: {type: DataTypes.STRING},
    amount: {type: DataTypes.DOUBLE},
    type: {type: DataTypes.ENUM('buy','payment')},
    date: {type: DataTypes.DATE},
    address: {type: DataTypes.STRING},
    status: {type: DataTypes.STRING, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Transaction;