const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const OtpLog = sequelize.define('otp_logs', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    phone: {type: DataTypes.STRING},
    otp: {type: DataTypes.INTEGER},
    otp_expired: {type: DataTypes.DATE},
    content: {type: DataTypes.STRING},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = OtpLog;