const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Suborder = sequelize.define('suborders', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    order_id: {type: DataTypes.STRING},
    suborder_id: {type: DataTypes.STRING},
    customer_id: {type: DataTypes.STRING},
    total: {type: DataTypes.DOUBLE},
    order_date: {type: DataTypes.DATE},
    order_status: {type: DataTypes.STRING},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Suborder;