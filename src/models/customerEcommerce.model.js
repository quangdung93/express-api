const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const customerEcommerce = sequelize.define('ecommerce_customers', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    first_name: {type: DataTypes.STRING},
    last_name: {type: DataTypes.STRING},
    customer_id: {type: DataTypes.STRING},
    telephone: {type: DataTypes.STRING},
    email: {type: DataTypes.STRING},
    status: {type: DataTypes.INTEGER, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = customerEcommerce;