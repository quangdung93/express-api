const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Banner = sequelize.define('banners', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    title: {type: DataTypes.STRING},
    content: {type: DataTypes.STRING},
    image: {type: DataTypes.STRING},
    url: {type: DataTypes.STRING},
    status: {type: DataTypes.STRING, defaultValue: 1},
    created_at: {type: DataTypes.DATE},
    updated_at: {type: DataTypes.DATE},
},{
    timestamps: false
});

module.exports = Banner;