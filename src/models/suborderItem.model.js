const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const suborderItem = sequelize.define('suborder_items', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    order_id: {type: DataTypes.STRING},
    suborder_id: {type: DataTypes.STRING},
    product_id: {type: DataTypes.STRING},
    sku: {type: DataTypes.STRING},
    name: {type: DataTypes.STRING},
    qty: {type: DataTypes.INTEGER},
    price: {type: DataTypes.DOUBLE},
},{
    timestamps: false
});

module.exports = suborderItem;