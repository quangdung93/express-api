const Joi = require('joi');
const responseFormat = require('../../utils/responseFormat');

const createCustomerSchema = Joi.object({
    name: Joi.string().required().messages({
        'string.base': 'Name must be a string',
        'any.required': 'Bạn chưa nhâp tên',
    }),
    username: Joi.string().required().messages({
        'string.base': 'UserName must be a string',
        'any.required': 'Bạn chưa nhập username',
    }),
    phone: Joi.string().required().messages({
        'string.base': 'Phone must be a string',
        'any.required': 'Bạn chưa nhập số điện thoại',
    }),
    email: Joi.string().email().required().messages({
        'string.base': 'Email must be a string',
        'string.email': 'Email không đúng định dạng',
        'any.required': 'Bạn chưa nhập email',
    })
});

const validateCreateCustomer = (req, res, next) => {
    const { error } = createCustomerSchema.validate(req.body);

    if (error) {
        return res.status(400).json(responseFormat.error(error.details[0].message, 400));
    }

    next();
};

module.exports = { validateCreateCustomer };