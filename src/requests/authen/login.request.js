const Joi = require('joi');
const responseFormat = require('../../utils/responseFormat');

const loginSchema = Joi.object({
    username: Joi.string().required().messages({
        'any.required': 'Username is required',
    }),
    password: Joi.string().min(6).required().messages({
        'any.required': 'Password is required',
    }),
    device_token: Joi.string().required().messages({
        'any.required': 'Device token is required',
    }),
});

const validateLogin = (req, res, next) => {
    const { error } = loginSchema.validate(req.body);

    if (error) {
        return res.status(400).json(responseFormat.error(error.details[0].message, 400));
    }

    next();
};

module.exports = { validateLogin };