// user.controller.js
const customerService = require('../services/customer.service');
const responseFormat = require('../utils/responseFormat');

exports.getAllAction = async (req, res, next) => {
    try {
        const users = await customerService.getAll();
        let userData = await customerService.getUserData();
        res.status(200).json(responseFormat.success({users, userData}));
    } catch (err) {
        next(err)
    }
};

exports.getByIdAction = async (req, res, next) => {
    const userId = req.params.id;
    
    try {
        const user = await customerService.getById(userId);
        res.status(200).json(responseFormat.success(user));
    } catch (error) {
        next(error)
    }
};

exports.createAction = async (req, res, next) => {
    const userData = req.body;

    try {
        const newCustomer = await customerService.create(userData);
        res.status(200).json(responseFormat.success(newCustomer));
    } catch (err) {
        next(err)
    }
};

exports.updateAction = async (req, res, next) => {
    const userId = req.params.id;
    const userData = req.body;

    try {
        const updatedUser = await customerService.update(userId, userData);
        res.status(200).json(responseFormat.success(updatedUser));
    } catch (err) {
        next(err)
    }
};



exports.deleteAction = async (req, res, next) => {
    const userId = req.params.id;

    try {
        await customerService.delete(userId);
        res.status(200).json(responseFormat.success());
    } catch (err) {
        next(err)
    }
};
