const customerService = require('../services/customer.service');
const authenService = require('../services/authen.service');
const responseFormat = require('../utils/responseFormat');
const jwt = require('jsonwebtoken');

// Xử lý logic để kiểm tra thông tin đăng nhập
exports.login = async (req, res) => {
    const { username, password, device_token } = req.body;

    // Kiểm tra thông tin đăng nhập và lấy thông tin người dùng
    const validCustomer = await authenService.authenticateUser(username, password, device_token);

    // Tạo payload cho JWT
    const payload = {
        user_id: validCustomer.id,
        username: validCustomer.username
    };

    // Tạo JWT
    const options = {expiresIn: '24h'};
    const token = jwt.sign(payload, process.env.SECRET_KEY, options);

    let dataLogin = {user: validCustomer, access_token: token}

    // Trả về JWT
    res.status(200).json(responseFormat.success(dataLogin, 'Đăng nhập thành công!'));
};

//
exports.refreshTokenAction = async (req, res) => {
    const refreshToken = req.body.refresh_token;

    let dataToken = await authenService.refreshToken(refreshToken);
    // delete dataToken.user;

    res.status(200).json(responseFormat.success(dataToken));
};

//Đăng ký tài khoản
exports.register = async (req, res) => {
    const customerData = req.body;
    const newCustomer = (await customerService.create(customerData)).get();
    delete newCustomer.password; //Xóa password khỏi kết quả trả về

    // Generate Token
    let dataRegister = await authenService.generateToken(newCustomer);

    res.status(200).json(responseFormat.success(dataRegister));
};

// Tạo OTP Login
exports.generateOtpAction = async (req, res) => {
    const { phone } = req.body;

    const generateOtp = await authenService.generateOtp(phone);

    if(generateOtp){
        res.status(200).json(responseFormat.success(null, 'Gửi OTP thành công!'));
    }
    else{
        res.status(200).json(responseFormat.error('Gửi OTP thất bại!'));
    }

};


// Verify OTP Login
exports.verifyOtpAction = async (req, res) => {
    const { phone, otp } = req.body;

    const customer = await authenService.verifyOtp(phone, otp);

    // Generate Token
    let dataLogin = await authenService.generateToken(customer);

    res.status(200).json(responseFormat.success(dataLogin, 'Xác thực OTP thành công!'));
};


//Tạo OTP register
exports.generateOtpRegisterAction = async (req, res) => {
    const { phone } = req.body;

    const generateOtp = await authenService.generateOtpRegister(phone);

    if(generateOtp){
        res.status(200).json(responseFormat.success(null, 'Gửi OTP thành công!'));
    }
    else{
        res.status(200).json(responseFormat.error('Gửi OTP thất bại!'));
    }

};


// Verify OTP Register
exports.verifyOtpRegisterAction = async (req, res) => {
    const { phone, otp } = req.body;

    await authenService.verifyOtpRegister(phone, otp);

    res.status(200).json(responseFormat.success(null, 'Xác thực OTP thành công!'));
};

