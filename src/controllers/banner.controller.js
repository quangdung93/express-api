const bannerService = require('../services/banner.service');
const responseFormat = require('../utils/responseFormat');

exports.getAllAction = async (req, res, next) => {
    try {
        const banners = await bannerService.getAll();
        res.status(200).json(responseFormat.success(banners));
    } catch (err) {
        next(err)
    }
};

exports.getByIdAction = async (req, res, next) => {
    const userId = req.params.id;
    
    try {
        const user = await bannerService.getById(userId);
        res.status(200).json(responseFormat.success(user));
    } catch (error) {
        next(error)
    }
};

exports.createAction = async (req, res, next) => {
    const userData = req.body;

    try {
        const newCustomer = await bannerService.create(userData);
        res.status(200).json(responseFormat.success(newCustomer));
    } catch (err) {
        next(err)
    }
};

exports.updateAction = async (req, res, next) => {
    const userId = req.params.id;
    const userData = req.body;

    try {
        const updatedUser = await bannerService.update(userId, userData);
        res.status(200).json(responseFormat.success(updatedUser));
    } catch (err) {
        next(err)
    }
};

exports.deleteAction = async (req, res, next) => {
    const userId = req.params.id;

    try {
        await bannerService.delete(userId);
        res.status(200).json(responseFormat.success());
    } catch (err) {
        next(err)
    }
};
