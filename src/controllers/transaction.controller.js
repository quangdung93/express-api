const transactionService = require('../services/transaction.service');
const responseFormat = require('../utils/responseFormat');

exports.getAllAction = async (req, res, next) => {
    try {
        const transactions = await transactionService.getAll();
        res.status(200).json(responseFormat.success(transactions));
    } catch (err) {
        next(err)
    }
};

