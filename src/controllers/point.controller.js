const pointService = require('../services/point.service');
const responseFormat = require('../utils/responseFormat');

exports.accumulatePointAction = async (req, res, next) => {
    const params = req.body;

    try {

        const accumulatePoint = await pointService.verifyPoint(params);

        res.status(200).json(responseFormat.success(accumulatePoint, 'Tích điểm thành công!'));
    } catch (err) {
        next(err)
    }
};

exports.getPointAction = async (req, res, next) => {
    const params = req.body;

    try {
        const point = await pointService.getPoint(params.customer_id);

        res.status(200).json(responseFormat.success(point));
    } catch (err) {
        next(err)
    }
};