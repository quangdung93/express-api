const orderService = require('../services/order.service');
const responseFormat = require('../utils/responseFormat');

exports.getCustomerOrderAction = async (req, res, next) => {
    const params = req.body;

    try {
        const orders = await orderService.getCustomerOrder(params);
        res.status(200).json(responseFormat.success(orders));
    } catch (err) {
        next(err)
    }
};

exports.getSubOrderAction = async (req, res, next) => {
    const params = req.body;

    try {
        const subOrder = await orderService.getSubOrder(params.suborder_id);
        res.status(200).json(responseFormat.success(subOrder));
    } catch (err) {
        next(err)
    }
};


