const storeService = require('../services/store.service');
const responseFormat = require('../utils/responseFormat');

exports.getAllAction = async (req, res, next) => {
    const params = req.body;

    try {
        const stores = await storeService.getAll(params);
        res.status(200).json(responseFormat.success(stores));
    } catch (err) {
        next(err)
    }
};

