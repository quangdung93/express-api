const voucherService = require('../services/voucher.service');
const responseFormat = require('../utils/responseFormat');

exports.getAllAction = async (req, res, next) => {
    try {
        const vouchers = await voucherService.getAll();
        res.status(200).json(responseFormat.success(vouchers));
    } catch (err) {
        next(err)
    }
};

