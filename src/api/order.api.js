const apiClient = require('../utils/apiClient');
const store = require('../store');
const authenApi = require('./authen.api');
const logging = require('../../logging');

function getClient(access_token) {
    return apiClient.create({
        baseURL: process.env.FAHASA_API_DOMAIN,
        headers: {
            'authorization': `Bearer ${access_token}`,
            'content-type': 'application/json'
        },
    });
}

async function getCustomerOrderApi(telephone) {
    const access_token = await authenApi.createJwt();
    const fahasaApiClient = getClient(access_token);
    const enpointUrl = 'ordersv/customer';

    try {
        let payload = {
            telephone: telephone
        };

        const response = await fahasaApiClient.post(`${enpointUrl}`, payload);

        let result = response.data;

        logging.info(`POST ${enpointUrl}`, {context: {request: payload, response: result}});

        return result;

    } catch (error) {
        console.error(`POST ${enpointUrl}`, error.message);
        throw error;
    }
}

async function getSubOrderApi(suborder_id) {
    const access_token = await authenApi.createJwt();
    const fahasaApiClient = getClient(access_token);

    const enpointUrl = 'ordersv/suborder';

    try {
        let payload = {
            suborder_id: suborder_id
        };

        const response = await fahasaApiClient.post(enpointUrl, payload);

        let result = response.data;

        logging.info(`POST ${enpointUrl}`, {context: {request: payload, response: result}});

        return result;

    } catch (error) {
        console.error(`Error ${enpointUrl}`, error.message);
        throw error;
    }
}

async function getOrderApi(order_id) {
    const access_token = await authenApi.createJwt();
    const fahasaApiClient = getClient(access_token);

    const enpointUrl = 'ordersv/order';

    try {
        let payload = {
            order_id: order_id
        };

        const response = await fahasaApiClient.post(enpointUrl, payload);

        let result = response.data;

        logging.info(`POST ${enpointUrl}`, {context: {request: payload, response: result}});

        return result;

    } catch (error) {
        console.error(`Error ${enpointUrl}`, error.message);
        throw error;
    }
}

module.exports = {
    getCustomerOrderApi, getSubOrderApi, getOrderApi
};
