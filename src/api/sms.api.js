const apiClient = require('../utils/apiClient');
const store = require('../store');
const authenApi = require('./authen.api');

async function sendSms(params) {
    const {phone, content} = params;
    const access_token = await authenApi.getAccessToken();
    try {
        let info = {
            phone,
            content,
            from: "web",
            action: "activate refer_code"
        };

        let payload = {
            userId: "callcenter@fahasa.com",
            token: access_token, 
            info: JSON.stringify(info)
        };

        const response = await apiClient.post(`api/sendSmsBrandName`, payload);

        let result = response.data;

        if(result.errorCode != 'undefined' && result.errorCode === 0){
            return true;
        }
        else{
            logging.error(`POST api/sendSmsBrandName`, {context: {request: payload, response: result}});
            return false;
        }

    } catch (error) {
        console.error('Error fetching user data:', error.message);
        throw error;
    }
}

module.exports = {
    sendSms,
};
