const apiClient = require('../utils/apiClient');
const store = require('../store');
const crypto = require('crypto');
const fs = require('fs');

async function getAccessToken() {
    try {
        const response = await apiClient.post(`api/authenticate`, {
            userId: "callcenter@fahasa.com",
            password: "1e775fcce0387f24014121f183f6cc7b"
        });

        let result = response.data;

        if(result.errorCode === 0){
            store.access_token = result?.data?.token;

            return result?.data?.token;
        }

        return false;

    } catch (error) {
        console.error('Error fetching user data:', error.message);
        throw error;
    }
}

async function createJwt() {
    try {
        const privateKey = fs.readFileSync('./rsa-key.pem', 'utf8');

        const jwtHeader = {
            alg: 'RS256',
            typ: 'JWT',
        };
        
        const jwtPayload = {
            telephone: '0123456789',
        };

        const base64UrlHeader = Buffer.from(JSON.stringify(jwtHeader)).toString('base64')
                                        .replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_');

        const base64UrlPayload = Buffer.from(JSON.stringify(jwtPayload)).toString('base64')
                                        .replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_');
    
        const data = base64UrlHeader + '.' + base64UrlPayload;
        const signer = crypto.createSign('RSA-SHA256');
        signer.update(data);

        const signature = signer.sign(privateKey, 'base64');
        const base64UrlSignature = signature.replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_');

        return data + '.' + base64UrlSignature;

    } catch (error) {
        console.error('Error createJwt:', error.message);
        throw error;
    }
}

module.exports = {
    getAccessToken, createJwt
};
