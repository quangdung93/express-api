const apiClient = require('../utils/apiClient');

async function fetchUserData() {
    try {
        const response = await apiClient.get(`users`);
        return response.data;
    } catch (error) {
        console.error('Error fetching user data:', error.message);
        throw error;
    }
}

module.exports = {
    fetchUserData,
};
