const mongoose = require('mongoose');
const connection = require('./connection');

// Định nghĩa schema cho mô hình người dùng
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

// Tạo và xuất mô hình người dùng
const User = connection.model('User', userSchema);
module.exports = User;
