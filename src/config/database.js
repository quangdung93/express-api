const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: 'localhost',
    dialect: 'mysql', // Có thể thay đổi đối với cơ sở dữ liệu khác
    logging: false,
    timezone: '+07:00',
    dialectOptions: {
        // useUTC: false, //for reading from database
        dateStrings: true,
        typeCast: true
    },
});

// sequelize.sync({ alter: true }).then(() => {
//     console.log('Database synchronized successfully.');
// });

module.exports = sequelize;