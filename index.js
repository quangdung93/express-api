const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const port = 3000;

// Sử dụng biến môi trường trong ứng dụng
const envFilePath = `.env.${process.env.NODE_ENV || 'development'}`;
const env = dotenv.config({ path: envFilePath });
dotenvExpand.expand(env); // Global process.env

const router = require('./src/routes/index');
const errorHandler = require('./src/middlewares/errorHandler');

// Sử dụng body-parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

// Sử dụng middleware cors
app.use(cors({
    origin: 'http://example.com', //Cho phép domain nào được gọi
    methods: ['GET', 'POST'], //Cho phép phương thức được gọi
    allowedHeaders: ['Content-Type', 'Authorization'] // Bắt buộc phải có Content-Type':'Authorization' trong Header
}));

// Sử dụng router
app.use('/api', router);

// Middleware xử lý lỗi
app.use(errorHandler);

// Khởi động server
// const server = app.listen(port, () => {
//     console.log(`Server is running on port ${port}`);
// });

const server = app.listen(port, () => {
    console.log(`Server running at Port ${port}`);
});

module.exports = server;